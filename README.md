BRDF Tools
==========

This repository contains various tools that I've used for loading BRDF

Mitsuba
-------
Implementation of BRDF for Mitsuba. You must edit in `src/bsdfs` the `SConscript` to add the plugins

### MERL
Merl materials are available for download [here](https://www.merl.com/brdf/).

To add the plugin to Mitsuba, copy in Mitsuba git folder
- `mitsuba/src/bsdfs/merl.cpp` to `src/bsdfs/` 
- create `3rdparty/MERL` in `src/bsdfs/` and copy in `mitsuba/src/bsdfs/3rdparty/MERL/BRDFRead.cpp`

Then in Mitsuba git folder edit `src/bsdfs/SConscript` and add:

```python
plugins += env.SharedLibrary('merl', ['merl.cpp'])
```

Then run `scons`.

To create a bsdf using MERL material in your Mitsuba XML scene, you can do:

```xml
<bsdf type="merl">
    <string name="filename" value="blue-metallic-paint.binary"/>
</bsdf>
```


### BTF
UBO2003 and UTIA (BTF database and MAM2014) materials are available for download:
- [UBO2003](http://cg.cs.uni-bonn.de/en/projects/btfdbb/download/ubo2003/)
- [UTIA BTF](http://btf.utia.cas.cz/?btf_dwn)
- [UTIA MAM2014](http://btf.utia.cas.cz/?btf_mam2014)

To add the plugin to Mitsuba, copy in Mitsuba git folder
- `mitsuba/src/bsdfs/btf.cpp` to `src/bsdfs/` 
- create `3rdparty/BTF` in `src/bsdfs/` and copy in `mitsuba/src/bsdfs/3rdparty/BTF`

Then in Mitsuba git folder edit `src/bsdfs/SConscript` and add:

```python
plugins += env.SharedLibrary('btf',
                             [
                                 'btf.cpp',
                                 '3rdparty/BTF/BTFLoad.cpp',
                                 '3rdparty/BTF/lodepng/lodepng.cpp',
                                 '3rdparty/BTF/BIG/TBIG.cpp',
                                 '3rdparty/BTF/BIG/TAlloc.cpp',
                                 '3rdparty/BTF/BIG/readexr.cpp',
                             ])
```

Then run `scons`.

To create a bsdf using MERL material in your Mitsuba XML scene, you can do:

```xml
<bsdf type="btf">
    <string name="filename" value="ubo_wallpaper"/>
	<string name="type" value="jpg"/>
</bsdf>
```
