#!/usr/bin/env python3

import os, sys
import shutil

def add_merl(mitsuba_path):
    print('Adding MERL')
    
    destdir_bsdf = os.path.join('src', 'bsdfs')

    # Adding mitsuba bsdf implementation
    shutil.copy(os.path.join(destdir_bsdf, 'merl.cpp'),
                os.path.join(mitsuba_path, destdir_bsdf)
    )

    # Adding 3rd party directory
    orig_3rdparty_path = os.path.join(destdir_bsdf, '3rdparty', 'MERL')
    dest_3rdparty_path = os.path.join(mitsuba_path, destdir_bsdf, '3rdparty', 'MERL')

    try:
        shutil.copytree(orig_3rdparty_path, dest_3rdparty_path)
    except FileExistsError as err:
        answer = input('Directory 3rdparty/MERL already exists, replace? [y/N] ')
        if answer == 'y' or answer == 'Y':
            shutil.rmtree(dest_3rdparty_path)
            shutil.copytree(orig_3rdparty_path, dest_3rdparty_path)

    print('Done!')
    
def add_btf(mitsuba_path):
    print('Adding BTF')
    
    destdir_bsdf = os.path.join('src', 'bsdfs')

    # Adding mitsuba bsdf implementation
    shutil.copy(os.path.join(destdir_bsdf, 'btf.cpp'),
                os.path.join(mitsuba_path, destdir_bsdf)
    )

    # Adding 3rd party directory
    orig_3rdparty_path = os.path.join(destdir_bsdf, '3rdparty', 'BTF')
    dest_3rdparty_path = os.path.join(mitsuba_path, destdir_bsdf, '3rdparty', 'BTF')

    try:
        shutil.copytree(orig_3rdparty_path, dest_3rdparty_path)
    except FileExistsError as err:
        answer = input('Directory 3rdparty/BTF already exists, replace? [y/N] ')
        if answer == 'y' or answer == 'Y':
            shutil.rmtree(dest_3rdparty_path)
            shutil.copytree(orig_3rdparty_path, dest_3rdparty_path)

    print('Done!')

def usage():
    print('Usage:')
    print('./add_to_mitsuba.sh <mitsuba_path> [merl] [btf]')
    exit(0)

def mitsuba_path_correct(mitsuba_path):
    # Rudimentary test to avoid using an incorrect path
    return os.path.isfile(os.path.join(mitsuba_path, 'SConstruct'))
    
if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
    else:
        mitsuba_path = sys.argv[1]
        if not mitsuba_path_correct(mitsuba_path):
            print('Provided path "' + mitsuba_path + '" is incorrect')
            exit(0)
        if len(sys.argv) == 2:
            add_merl(mitsuba_path)
            add_btf(mitsuba_path)
        else:
            for a in sys.argv[2:]:
                if a == 'merl':
                    add_merl(mitsuba_path)
                elif a == 'btf':
                    add_btf(mitsuba_path)
                else:
                    print('Unknown plugin specified.')
                    usage()
                    
        print('Plugin(s) added. Now, run scons from ' + mitsuba_path)
