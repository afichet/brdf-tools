Date: 2017-March-03
Authors: M. Havlicek, J. Filip, R. Vavra
Email: filipj@utia.cas.cz

BIG data format for multiple image encapsulation

features:	- fast data loading to memory
    			- fast memory access or seeking from HDD (if stored data exceeds memory)
application:	- BTF data
		      		- dynamic textures

----------------------
Files
----------------------

TBIG.cpp - BIG format source
TBIG.cpp - BIG format header
bigtest.cpp - main file
lodepng.cpp - files for reading of PNG
lodepng.h  
readexr.cpp - files for reading of EXR
readexr.h
Makefile - please edit USRDIR

----------------------
Compilation.
----------------------

make libexr
make 

----------------------
Creating a new file
----------------------

./bigtest -s script.txt bigname

where: 
script.txt - TXT file that specifies image files to be loaded (example attached) 
bigname - name of the created file

----------------------
Reading file & values look-up
----------------------

in your code:

//loading file to memory 
TBIG B;
B.load(bigname,true); // true = memory, false = disk
.
.
.
// in lookup loop
B.get_pixel(0,y,x, imgIndex, aux);

// specicly for BTF rendering using UBO angular sampling using 81 directions
// y/x: row/column, viewPos/illuPos: values 0-80
B.get_pixel(0,y,x, 81*viewPos+illuPos, aux);



