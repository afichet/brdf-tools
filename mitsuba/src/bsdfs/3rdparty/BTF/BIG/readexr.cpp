
#include "readexr.h"

#include <ImfOutputFile.h>
#include <ImfInputFile.h>
#include <ImfChannelList.h>
#include <ImfStringAttribute.h>
#include <ImfMatrixAttribute.h>
#include <ImfArray.h>
#include <ImfRgbaFile.h>

#include <string>
#include <stdio.h>
#include <limits.h>
#include <cassert>
#include <iostream>

#define IMGIO_FILE_NOT_OPENED -1
#define IMGIO_NOT_A_PNG -2
#define IMGIO_ALLOCATION_PROBLEM -3
#define IMGIO_FILE_READ_PROBLEM -4
#define IMGIO_FILE_WRITE_PROBLEM -5
#define IMGIO_FILE_CONTENTS_PROBLEM -6
#define IMGIO_WRONG_PARAMETER -7
#define sz_2diw 6

using namespace Imf;
using namespace Imath;
using namespace ::std;

/**Checks if the given file is an OpenEXR file format.
 *
 *@param fileName input file name
 */
bool isThisAnOpenExrFile (const char fileName[])
{
    std::ifstream f (fileName, std::ios_base::binary);
    char b[4];
    f.read (b, sizeof (b));
    return !!f && b[0] == 0x76 && b[1] == 0x2f && b[2] == 0x31 && b[3] == 0x01;
}


/**Checks if the given file is a complete OpenEXR file.
 *
 *@param fileName input file name
 */
bool isComplete (const char fileName[])
{
    InputFile in (fileName);
    return in.isComplete();
}


/**Reads a OpenEXR file header, loads image size and number of planes.
 *
 *@param fileName input file name
 *@param planes output parameter, number of planes.
 *@param nr output parameter, image height
 *@param nc output parameter, image width
 *
 *@return 1 on success, otherwise: <code>IMGIO_FILE_NOT_OPENED</code>, <code>IMGIO_FILE_READ_PROBLEM</code>
 */
int read_OpenEXR_header(const char *filename, int *planes, int *nr, int *nc)
{
    InputFile *file = NULL;
    try {
        file = new InputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Box2i dw = file->header().dataWindow();
    const int width  = dw.max.x - dw.min.x + 1;
    const int height = dw.max.y - dw.min.y + 1;

    //size
    *nr = height;
    *nc = width;
    
    /*
    const ChannelList &channels = file->header().channels();
    
    ChannelList::ConstIterator it = channels.begin();
    
    int counter = 1;
    try {
        while (true) {
            it++;
            counter++;
        }
    } catch (...) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_READ_PROBLEM;
    }

    *planes = counter;
    */
    *planes=3;
    

    if (file != NULL)
        delete file;

    return 1;
} /* read_OpenEXR_header ------------------------------------------------------ */



/**Checks if listed channels are compatible with number of planes and other parameters.
 * Only for <code>read_OpenEXR_file_general()</code> method.
 *
 *@param channels list of channels from OpenEXR file header
 *@param planes number of planes.
 *@param asrgb save image as RGB parameter
 *
 *@return 1 on success, 0 otherwise
 */
int checkChannels(ChannelList const&channels, const int planes, const int asrgb)
{
    bool result = true;
    if (planes == 1 && !asrgb) {
        result = result && channels.findChannel("Y") != NULL;
    } else if (planes == 1 && asrgb) {
        result = result && channels.findChannel("R") != NULL;
    } else if (planes == 2 && asrgb) {
        result = result && channels.findChannel("R") != NULL;
        result = result && channels.findChannel("G") != NULL;
    } else if ((planes == 3 || planes == 4) && asrgb) {
        result = result && channels.findChannel("R") != NULL;
        result = result && channels.findChannel("G") != NULL;
        result = result && channels.findChannel("B") != NULL;
    } else if (planes == 3 && !asrgb) {
        result = result && channels.findChannel("X") != NULL;
        result = result && channels.findChannel("Y") != NULL;
        result = result && channels.findChannel("Z") != NULL;
    } else {
        //non standard
        for (int i = 0; i < planes; i++) {
            char s[400];
            snprintf(s, 400, "%d", i);
            result = result && channels.findChannel(s) != NULL;
        }
    }

    //alpha
    if (planes == 4) {
        result = result && channels.findChannel("A") != NULL;
    }

    return (result) ? 1 : 0;
} /* checkChannels ------------------------------------------------------ */


/**Reads the given OpenEXR file. It support more than 4 channels and it automaticaly detects
 * channels fomart (Y-luminance, RGB - RGB, RGBA - RGB+Alpha, channels named by their numbers)
 *
 *@param fileName input file name
 *@param arr output parameter, image data.
 *@param planes number of planes.
 *@param nr image height
 *@param nc image width
 *@param oneplane nonzero meas to save only one plane - plane number to save
 *
 *@return 1 on success, otherwise: <code>IMGIO_WRONG_PARAMETER</code>, <code>IMGIO_FILE_NOT_OPENED</code>,
 *<code>IMGIO_FILE_READ_PROBLEM</code>
 */
int read_OpenEXR_file_general(const char * filename, float ***arr, const int planes,
        const int nr, const int nc, const int oneplane)
{
    if (planes < 0) return IMGIO_WRONG_PARAMETER;
    float **arr2 = NULL;
    if (planes == 1) /* grayscale image */ {
        if (oneplane < 0) arr2 = (float**) arr;
        else arr2 = arr[oneplane];
    }

    InputFile *file = NULL;
    try {
        file = new InputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Box2i dw = file->header().dataWindow();
    const int width  = dw.max.x - dw.min.x + 1;
    const int height = dw.max.y - dw.min.y + 1;

    if (nc != width || nr != height) {
        if (file != NULL)
            delete file;
        return IMGIO_WRONG_PARAMETER;
    }

    int asrgb = 0;
    const ChannelList &channels = file->header().channels();
    const Channel *channelPtr = channels.findChannel("R");
    asrgb = channelPtr != NULL;
    if ( !checkChannels(channels, planes, asrgb) ) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_READ_PROBLEM;
    }

    try {
        int y = 0;
        while (dw.min.y <= dw.max.y) {
            FrameBuffer frameBuffer;
            if (planes == 1 && !asrgb) {
                frameBuffer.insert("Y", // name
                        Slice(FLOAT, // type
                        (char *) & arr2[y][0], // base
                        sizeof (float) * 1, // xStride
                        0));
            } else if (planes == 1 && asrgb) {
                frameBuffer.insert("R", // name
                        Slice(FLOAT, // type
                        (char *) & arr2[y][0], // base
                        sizeof (float) * 1, // xStride
                        0)); //don't skip anything, next row will be set manualy
            } else if (planes == 2 && asrgb) {
                frameBuffer.insert("R", // name
                        Slice(FLOAT, // type
                        (char *) & arr[0][y][0], // base
                        sizeof (float) * 1, // xStride
                        0)); //don't skip anything, next row will be set manualy
                frameBuffer.insert("G", // name
                        Slice(FLOAT, // type
                        (char *) & arr[1][y][0], // base
                        sizeof (float) * 1, // xStride
                        0));
            } else if (planes == 3 || planes == 4) {
                frameBuffer.insert("R", // name
                        Slice(FLOAT, // type
                        (char *) & arr[0][y][0], // base
                        sizeof (float) * 1, // xStride
                        0)); //don't skip anything, next row will be set manualy
                frameBuffer.insert("G", // name
                        Slice(FLOAT, // type
                        (char *) & arr[1][y][0], // base
                        sizeof (float) * 1, // xStride
                        0));
                frameBuffer.insert("B", // name
                        Slice(FLOAT, // type
                        (char *) & arr[2][y][0], // base
                        sizeof (float) * 1, // xStride
                        0));
            } else {
                //non standard
                for (int i = 0; i < planes; i++) {
                    char s[400];
                    snprintf(s, 400, "%d", i);
                    frameBuffer.insert(s, // name
                            Slice(FLOAT, // type
                            (char *) & arr[i][y][0], // base
                            sizeof (float) * 1, // xStride
                            0));
                }
            }

            //add alpha
            if (planes == 4) {
                frameBuffer.insert("A", // name
                        Slice(FLOAT, // type
                        (char *) & arr[3][y][0], // base
                        sizeof (float) * 1, // xStride
                        0));
            }			// fillValue

            file->setFrameBuffer (frameBuffer);
            file->readPixels (dw.min.y, min(dw.min.y, dw.max.y));

            y++;
            dw.min.y += 1;
        }
    } catch (...) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_READ_PROBLEM;
    }

    if (file != NULL)
        delete file;

    return 1;

} /* read_OpenEXR_file_general ------------------------------------------------------ */

int read_OpenEXR_file_xyz(const char * filename, float ***arr, const int planes,
        const int nr, const int nc, const int oneplane)
{
    if (planes < 3 || planes > 3) return IMGIO_WRONG_PARAMETER;

    InputFile *file = NULL;
    try {
        file = new InputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Box2i dw = file->header().dataWindow();
    const int width  = dw.max.x - dw.min.x + 1;
    const int height = dw.max.y - dw.min.y + 1;
    //    printf("%d %d %d %d\n",nr,nc,height,width);

    if (nc != width || nr != height) {
        if (file != NULL)
            delete file;
        return IMGIO_WRONG_PARAMETER;
    }

    Array2D<half> X, Y, Z;
    X.resizeErase(nr,nc);
    Y.resizeErase(nr,nc);
    Z.resizeErase(nr,nc);
    
    FrameBuffer frameBuffer;
    try {
      frameBuffer.insert("X",                       // name
                        Slice(HALF,               // type
                        (char *) & X[0][0],       // base
                        sizeof (X[0][0]) * 1,     // xStride
                        sizeof (X[0][0]) * width, // yStride
                        1, 1,
                        0.0));
      frameBuffer.insert("Y",                       // name
                        Slice(HALF,               // type
                        (char *) & Y[0][0],       // base
                        sizeof (Y[0][0]) * 1,     // xStride
                        sizeof (Y[0][0]) * width, // yStride
                        1, 1,
                        0.0));
      frameBuffer.insert("Z",                       // name
                        Slice(HALF,               // type
                        (char *) & Z[0][0],       // base
                        sizeof (Z[0][0]) * 1,     // xStride
                        sizeof (Z[0][0]) * width, // yStride
                        1, 1,
                        0.0));

      file->setFrameBuffer (frameBuffer);
      file->readPixels (dw.min.y, dw.max.y);

    } catch (...) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_READ_PROBLEM;
    }
     
    if (file != NULL)
        delete file;

    for(int irow=0;irow<nr;irow++)
        for(int jcol=0;jcol<nc;jcol++){
	        arr[0][irow][jcol] = (float)X[irow][jcol];
    	    arr[1][irow][jcol] = (float)Y[irow][jcol];
    	    arr[2][irow][jcol] = (float)Z[irow][jcol];
    }

    return 1;

} /* read_OpenEXR_file_xyz ------------------------------------------------------ */

int read_OpenEXR_file_xyz_FLOAT(const char * filename, float ***arr, const int planes,
        const int nr, const int nc, const int oneplane)
{
    if (planes < 3 || planes > 3) return IMGIO_WRONG_PARAMETER;

    InputFile *file = NULL;
    try {
        file = new InputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Box2i dw = file->header().dataWindow();
    const int width  = dw.max.x - dw.min.x + 1;
    const int height = dw.max.y - dw.min.y + 1;

    if (nc != width || nr != height) {
        if (file != NULL)
            delete file;
        return IMGIO_WRONG_PARAMETER;
    }

    Array2D<float> X, Y, Z;
    X.resizeErase(nr,nc);
    Y.resizeErase(nr,nc);
    Z.resizeErase(nr,nc);
    
    FrameBuffer frameBuffer;

    try {
      frameBuffer.insert("X",                       // name
                        Slice(FLOAT,               // type
                        (char *) & X[0][0],       // base
                        sizeof (X[0][0]) * 1,     // xStride
                        sizeof (X[0][0]) * width, // yStride
                        1, 1,
                        0.0));
      frameBuffer.insert("Y",                       // name
                        Slice(FLOAT,               // type
                        (char *) & Y[0][0],       // base
                        sizeof (Y[0][0]) * 1,     // xStride
                        sizeof (Y[0][0]) * width, // yStride
                        1, 1,
                        0.0));
      frameBuffer.insert("Z",                       // name
                        Slice(FLOAT,               // type
                        (char *) & Z[0][0],       // base
                        sizeof (Z[0][0]) * 1,     // xStride
                        sizeof (Z[0][0]) * width, // yStride
                        1, 1,
                        0.0));

      file->setFrameBuffer (frameBuffer);
      file->readPixels (dw.min.y, dw.max.y);

    } catch (...) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_READ_PROBLEM;
    }
     
    if (file != NULL)
        delete file;

    for(int irow=0;irow<nr;irow++)
        for(int jcol=0;jcol<nc;jcol++){
	        arr[0][irow][jcol] = (float)X[irow][jcol];
    	    arr[1][irow][jcol] = (float)Y[irow][jcol];
    	    arr[2][irow][jcol] = (float)Z[irow][jcol];
    }

    return 1;

} /* read_OpenEXR_file_xyz_FLOAT ------------------------------------------------------ */

int write_OpenEXR_file_rgb_FLOAT(const char *filename, float const*const*const*arr, const int planes,
        const int nr, const int nc, const int oneplane, const int asrgb)
{
    const int width = nc;
    const int height = nr;

    if (planes < 3 || planes > 3) return IMGIO_WRONG_PARAMETER;
    if (asrgb) return IMGIO_WRONG_PARAMETER;

    Header *header = new Header(width, height);
    //    header.dataWindow() = dataWindow;

    header->channels().insert("R", Channel(FLOAT));
    header->channels().insert("G", Channel(FLOAT));
    header->channels().insert("B", Channel(FLOAT));

    OutputFile *file = NULL;
    try {
        file = new OutputFile(filename, *header);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Array2D<float>R,G,B;
    R.resizeErase(nr,nc);
    G.resizeErase(nr,nc);
    B.resizeErase(nr,nc);

    for(int irow=0;irow<nr;irow++)
        for(int jcol=0;jcol<nc;jcol++){
	        R[irow][jcol] = (float)arr[0][irow][jcol];
    	    G[irow][jcol] = (float)arr[1][irow][jcol];
    	    B[irow][jcol] = (float)arr[2][irow][jcol];
    }

    FrameBuffer frameBuffer;
    
    try {
      frameBuffer.insert("R",                       // name
                        Slice(FLOAT,               // type
                        (char *) & R[0][0],       // base
                        sizeof(R[0][0]) * 1,     // xStride
                              sizeof(R[0][0]) * width)); // yStride
      frameBuffer.insert("G",                       // name
                        Slice(FLOAT,               // type
                        (char *) & G[0][0],       // base
                        sizeof(G[0][0]) * 1,     // xStride
                              sizeof(G[0][0]) * width)); // yStride
      frameBuffer.insert("B",                       // name
                        Slice(FLOAT,               // type
                        (char *) & B[0][0],       // base
                        sizeof(B[0][0]) * 1,     // xStride
                              sizeof(B[0][0]) * width)); // yStride

      file->setFrameBuffer(frameBuffer);
      file->writePixels(height);

    } catch (...) {
        if (file != NULL)
            delete file;
        delete header;
        return IMGIO_FILE_WRITE_PROBLEM;
    }

    if (file != NULL)
        delete file;
    delete header;

    return 1;
    
} /* write_OpenEXR_file_rgb_FLOAT ---------------------------------------------------- */


/**Reads the given OpenEXR file. It uses standard RGBA interface and reads data through half floats
 * (16 bit floats).
 * Supported channles fomarts are Y-luminance, RGB - RGB, RGBA - RGB+Alpha.
 *
 *@param fileName input file name
 *@param arr output parameter, image data.
 *@param planes number of planes.
 *@param nr image height
 *@param nc image width
 *@param oneplane nonzero mean to save only one plane - plane number to save
 *
 *@return 1 on success, otherwise: <code>IMGIO_WRONG_PARAMETER</code>, <code>IMGIO_FILE_NOT_OPENED</code>,
 *<code>IMGIO_FILE_READ_PROBLEM</code>
 */
int read_OpenEXR_file_rgba(const char * filename, float ***arr, const int planes,
        const int nr, const int nc, const int oneplane)
{
    if (planes < 0 || planes > 4) return IMGIO_WRONG_PARAMETER;

    float **arr2 = NULL;
    if (planes == 1) /* grayscale image */ {
        if (oneplane < 0) arr2 = (float**) arr;
        else arr2 = arr[oneplane];
    }

    //
    // Read an RGBA image using class RgbaInputFile.
    // Read the pixels, 1 scan lines at a time, and
    // store the pixel data in a buffer that is just
    // large enough to hold 1 scan lines worth of data.
    //
    //	- open the file
    //	- allocate memory for the pixels
    //	- for each block of 1 scan lines,
    //	  describe the memory layout of the pixels,
    //	  read the pixels from the file,
    //	  process the pixels and discard them
    //

    RgbaInputFile *file = NULL;
    try {
        file = new RgbaInputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    try {
        Box2i dw = file->dataWindow();
        int width = dw.max.x - dw.min.x + 1;
        //int height = dw.max.y - dw.min.y + 1;
        Array2D<Rgba> pixels(1, width);

        int y = 0, x;
        while (dw.min.y <= dw.max.y) {
            file->setFrameBuffer(&pixels[0][0] - dw.min.x - dw.min.y * width,
                    1, width);

            file->readPixels(dw.min.y, min(dw.min.y, dw.max.y));

            if (planes == 1) {
                for (x = 0; x < nc; x++) {
                    arr2[y][x] = pixels[0][x].r;
                }
            }
            else if (planes == 2) {
                for (x = 0; x < nc; x++) {
                    arr[0][y][x] = pixels[0][x].r;
                    arr[1][y][x] = pixels[0][x].g;
                }
            }
            else if (planes == 3 || planes == 4){
                for (x = 0; x < nc; x++) {
                    arr[0][y][x] = pixels[0][x].r;
                    arr[1][y][x] = pixels[0][x].g;
                    arr[2][y][x] = pixels[0][x].b;
                }
            }
            
            if (planes == 4)
                for (x = 0; x < nc; x++) {
                    arr[3][y][x] = pixels[0][x].a;
                }

            y++;
            dw.min.y += 1;
        }
    } catch (...) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_READ_PROBLEM;
    }

    if (file != NULL)
        delete file;

    return 1;
} /* read_OpenEXR_file_rgba ------------------------------------------------------ */


/**Reads the given OpenEXR file. It automatically selects interface to read:
 * 1) half floats stored in channles Y-luminance, RGB - RGB, RGBA RGB+Alpha
 * 2) floats stored in channles Y-luminance, RGB - RGB, RGBA RGB+Alpha, channels named by their numbers
 *
 *@param fileName input file name
 *@param arr output parameter, image data.
 *@param planes number of planes.
 *@param nr image height
 *@param nc image width
 *@param oneplane nonzero means to save only one plane - plane number to save
 *
 *@return 1 on success, otherwise: <code>IMGIO_WRONG_PARAMETER</code>, <code>IMGIO_FILE_NOT_OPENED</code>,
 *<code>IMGIO_FILE_READ_PROBLEM</code>
 */
int read_OpenEXR_file(const char *filename, float ***arr, const int planes,
        const int nr, const int nc, const int oneplane)
{
    InputFile *file = NULL;
    try {
        file = new InputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    const ChannelList &channels = file->header().channels();
    int asrgb = 0;
    const Channel *channelPtr = channels.findChannel("R");
    asrgb = channelPtr != NULL;

    //use rgba interface if file consits of HALF FLOATS
    if(asrgb)
      if (channels.begin().channel().type == HALF) {
        if (file != NULL)
          delete file;
        return read_OpenEXR_file_rgba(filename, arr, planes, nr, nc, oneplane);
      }
      else {
        if (file != NULL)
          delete file;
        return read_OpenEXR_file_general(filename, arr, planes, nr, nc, oneplane);
      }
    else
      if (channels.begin().channel().type == HALF) {
        if (file != NULL)
          delete file;
        return read_OpenEXR_file_xyz(filename, arr, planes, nr, nc, oneplane);
      }    
      else {
        if (file != NULL)
          delete file;
        printf("Reading as XYZ float!\n");
        return read_OpenEXR_file_xyz_FLOAT(filename, arr, planes, nr, nc, oneplane);
      }

} /* read_OpenEXR_file ------------------------------------------------------ */


int write_OpenEXR_file_xyz(const char *filename, float const*const*const*arr, const int planes,
        const int nr, const int nc, const int oneplane, const int asrgb)
{
    const int width = nc;
    const int height = nr;

    if (planes < 3 || planes > 3) return IMGIO_WRONG_PARAMETER;
    if (asrgb) return IMGIO_WRONG_PARAMETER;

    Header *header = new Header(width, height);
    //    header.dataWindow() = dataWindow;

    header->channels().insert("X", Channel(HALF));
    header->channels().insert("Y", Channel(HALF));
    header->channels().insert("Z", Channel(HALF));

    OutputFile *file = NULL;
    try {
        file = new OutputFile(filename, *header);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Array2D<half>X,Y,Z;
    X.resizeErase(nr,nc);
    Y.resizeErase(nr,nc);
    Z.resizeErase(nr,nc);

    for(int irow=0;irow<nr;irow++)
        for(int jcol=0;jcol<nc;jcol++){
	        X[irow][jcol] = (half)arr[0][irow][jcol];
    	    Y[irow][jcol] = (half)arr[1][irow][jcol];
    	    Z[irow][jcol] = (half)arr[2][irow][jcol];
    }

    FrameBuffer frameBuffer;
    
    try {
      frameBuffer.insert("X",                       // name
                        Slice(HALF,               // type
                        (char *) & X[0][0],       // base
                        sizeof(X[0][0]) * 1,     // xStride
                              sizeof(X[0][0]) * width)); // yStride
      frameBuffer.insert("Y",                       // name
                        Slice(HALF,               // type
                        (char *) & Y[0][0],       // base
                        sizeof(Y[0][0]) * 1,     // xStride
                              sizeof(Y[0][0]) * width)); // yStride
      frameBuffer.insert("Z",                       // name
                        Slice(HALF,               // type
                        (char *) & Z[0][0],       // base
                        sizeof(Z[0][0]) * 1,     // xStride
                              sizeof(Z[0][0]) * width)); // yStride

      file->setFrameBuffer(frameBuffer);
      file->writePixels(height);

    } catch (...) {
        if (file != NULL)
            delete file;
        delete header;
        return IMGIO_FILE_WRITE_PROBLEM;
    }

    if (file != NULL)
        delete file;
    delete header;

    return 1;
    
} /* write_OpenEXR_file_xyz ---------------------------------------------------- */

int write_OpenEXR_file_xyz_FLOAT(const char *filename, float const*const*const*arr, const int planes,
        const int nr, const int nc, const int oneplane, const int asrgb)
{
    const int width = nc;
    const int height = nr;

    if (planes < 3 || planes > 3) return IMGIO_WRONG_PARAMETER;
    if (asrgb) return IMGIO_WRONG_PARAMETER;

    Header *header = new Header(width, height);
    //    header.dataWindow() = dataWindow;

    header->channels().insert("X", Channel(FLOAT));
    header->channels().insert("Y", Channel(FLOAT));
    header->channels().insert("Z", Channel(FLOAT));

    OutputFile *file = NULL;
    try {
        file = new OutputFile(filename, *header);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Array2D<float>X,Y,Z;
    X.resizeErase(nr,nc);
    Y.resizeErase(nr,nc);
    Z.resizeErase(nr,nc);

    for(int irow=0;irow<nr;irow++)
        for(int jcol=0;jcol<nc;jcol++){
	        X[irow][jcol] = (float)arr[0][irow][jcol];
    	    Y[irow][jcol] = (float)arr[1][irow][jcol];
    	    Z[irow][jcol] = (float)arr[2][irow][jcol];
    }

    FrameBuffer frameBuffer;
    
    try {
      frameBuffer.insert("X",                       // name
                        Slice(FLOAT,               // type
                        (char *) & X[0][0],       // base
                        sizeof(X[0][0]) * 1,     // xStride
                              sizeof(X[0][0]) * width)); // yStride
      frameBuffer.insert("Y",                       // name
                        Slice(FLOAT,               // type
                        (char *) & Y[0][0],       // base
                        sizeof(Y[0][0]) * 1,     // xStride
                              sizeof(Y[0][0]) * width)); // yStride
      frameBuffer.insert("Z",                       // name
                        Slice(FLOAT,               // type
                        (char *) & Z[0][0],       // base
                        sizeof(Z[0][0]) * 1,     // xStride
                              sizeof(Z[0][0]) * width)); // yStride

      file->setFrameBuffer(frameBuffer);
      file->writePixels(height);

    } catch (...) {
        if (file != NULL)
            delete file;
        delete header;
        return IMGIO_FILE_WRITE_PROBLEM;
    }

    if (file != NULL)
        delete file;
    delete header;

    return 1;
    
} /* write_OpenEXR_file_xyz_FLOAT ---------------------------------------------------- */


/**Writes the image into a OpenEXR format file. It uses standard RGBA interface, which saves data as half floats (16 bit floats) and supports maximally 4 planes images.
 *
 *@param fileName input file name
 *@param arr image data to write.
 *@param planes number of planes.
 *@param nr image height
 *@param nc image width
 *@param oneplane nonzero = save only one plane - plane number to save
 *@param asrgb number If channels is less or equal 4,  save it as RGB (RGBA) image. (Some HDR image viewers can read only RGB images.) Otherwise 1 plane images are saved as Y channel.
 *
 *@return 1 on success, otherwise: <code>IMGIO_WRONG_PARAMETER</code>, <code>IMGIO_FILE_NOT_OPENED</code>,
 *<code>IMGIO_FILE_WRITE_PROBLEM</code>
 */
int write_OpenEXR_file_rgba(const char *filename, float const*const*const*arr, const int planes,
        const int nr, const int nc, const int oneplane, const int asrgb)
{
    const size_t width = nc;
    const size_t height = nr;

    if (planes < 0) return IMGIO_WRONG_PARAMETER;
    if (planes > 4) return IMGIO_WRONG_PARAMETER;
    float const*const*arr2 = NULL;
    if (planes == 1) /* grayscale image */ {
        if (oneplane < 0) arr2 = (float**) arr;
        else arr2 = arr[oneplane];
    }

    Box2i displayWindow(V2i(0, 0), V2i(width - 1, height - 1));
    Box2i dataWindow(V2i(0, 0), V2i(width - 1, height - 1));
    RgbaOutputFile *file = NULL;

    try {
        if (planes == 1 && !asrgb) {
            file = new RgbaOutputFile(filename, displayWindow, dataWindow, WRITE_Y);
        } else if (planes == 2 || planes == 3 || (planes == 1 && asrgb)) {
            file = new RgbaOutputFile(filename, displayWindow, dataWindow, WRITE_RGB);
        } else if (planes == 4) {
            file = new RgbaOutputFile(filename, displayWindow, dataWindow, WRITE_RGBA);
        } else {
            printf("ERROR: Unsupported number of planes (OpenEXR).");
            return -1;
        }
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    try {
        Array2D<Rgba> pixels(1, width);
        file->setFrameBuffer(&pixels[0][0], 1, 0); //ystride is zero, because frame buffer consists just one line

        int y, x;
        /* Write 1 row at a time. */
        for (y = 0; y < nr; y++) {
            FrameBuffer frameBuffer;
            if (planes == 1) {
                for (x = 0; x < nc; x++) {
                    pixels[0][x].r = arr2[y][x];
                    pixels[0][x].g = arr2[y][x];
                    pixels[0][x].b = arr2[y][x];
                }
            } else if (planes == 2) {
                for (x = 0; x < nc; x++) {
                    pixels[0][x].r = arr[0][y][x];
                    pixels[0][x].g = arr[1][y][x];
                    pixels[0][x].b = 0;
                }
            } else if (planes == 3 || planes == 4) {
                for (x = 0; x < nc; x++) {
                    pixels[0][x].r = arr[0][y][x];
                    pixels[0][x].g = arr[1][y][x];
                    pixels[0][x].b = arr[2][y][x];
                }
            }

            //add alpha
            if (planes == 4) {
                for (x = 0; x < nc; x++) {
                    pixels[0][x].a = arr[3][y][x];
                }
            }

            //      lprintf("write_OpenEXR_file(): y=%d\n",y);
            fflush(stdout);
            file->writePixels(1);
        }
    } catch (...) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_WRITE_PROBLEM;
    }

    if (file != NULL)
        delete file;

    return 1;
} /* write_OpenEXR_file_rgba ---------------------------------------------------- */


int isFloatEXR(const char filename [])
{
    InputFile *file = NULL;
    try {
        file = new InputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    const ChannelList &channels = file->header().channels();

    if(isThisAnOpenExrFile(filename) && isComplete(filename))
      {  
        if (channels.begin().channel().type == HALF) 
        {
          if (file != NULL)
           delete file;
          return 0;
        }
        else
        {
          if (file != NULL)
           delete file;
          return 1;
        }
      }

    if (file != NULL)
        delete file;

    return -1;

} /* isFloatEXR ------------------------------------------------------ */



int read_OpenEXR_file_xyz_half(const char * filename, char * arr, const int planes, const int nr, const int nc)
{
    if (planes < 3 || planes > 3) return IMGIO_WRONG_PARAMETER;

    InputFile *file = NULL;
    try {
        file = new InputFile(filename);
    } catch (...) {
        return IMGIO_FILE_NOT_OPENED;
    }

    Box2i dw = file->header().dataWindow();
    const int width  = dw.max.x - dw.min.x + 1;
    const int height = dw.max.y - dw.min.y + 1;

    if (nc != width || nr != height) {
        if (file != NULL)
            delete file;
        return IMGIO_WRONG_PARAMETER;
    }

    Array2D<half> X, Y, Z;
    X.resizeErase(nr,nc);
    Y.resizeErase(nr,nc);
    Z.resizeErase(nr,nc);
    
    FrameBuffer frameBuffer;
    try {
      frameBuffer.insert("X",                       // name
                        Slice(HALF,               // type
                        (char *) & X[0][0],       // base
                        sizeof (X[0][0]) * 1,     // xStride
                        sizeof (X[0][0]) * width, // yStride
                        1, 1,
                        0.0));
      frameBuffer.insert("Y",                       // name
                        Slice(HALF,               // type
                        (char *) & Y[0][0],       // base
                        sizeof (Y[0][0]) * 1,     // xStride
                        sizeof (Y[0][0]) * width, // yStride
                        1, 1,
                        0.0));
      frameBuffer.insert("Z",                       // name
                        Slice(HALF,               // type
                        (char *) & Z[0][0],       // base
                        sizeof (Z[0][0]) * 1,     // xStride
                        sizeof (Z[0][0]) * width, // yStride
                        1, 1,
                        0.0));

      file->setFrameBuffer (frameBuffer);
      file->readPixels (dw.min.y, dw.max.y);

    } catch (...) {
        if (file != NULL)
            delete file;
        return IMGIO_FILE_READ_PROBLEM;
    }
     
    if (file != NULL)
        delete file;

    int halffloatsize=2;

    for(int i=0;i<nr;i++)
        for(int j=0;j<nc;j++)
        {
            memcpy(arr+3*nc*i*halffloatsize+3*j*halffloatsize,&(X[i][j]),halffloatsize);
            memcpy(arr+3*nc*i*halffloatsize+3*j*halffloatsize+halffloatsize,&(Y[i][j]),halffloatsize);
            memcpy(arr+3*nc*i*halffloatsize+3*j*halffloatsize+2*halffloatsize,&(Z[i][j]),halffloatsize);
        }

    return 1;

} /* read_OpenEXR_file_xyz_half ------------------------------------------------------ */
