/*
 * BTFLoad.cpp
 *
 *  Created on: Sep 7, 2017
 *      Author: afichet
 */

#include "BTFLoad.h"

// BIG handling
#include "BIG/TBIG.h"

// PNG handling
#include "lodepng/lodepng.h"

// EXR handling
#include <ImfRgbaFile.h>
#include <ImfArray.h>

// JPEG handling
#include "CImg/CImg.h"

#include <unistd.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define CLAMP(a, minV, maxV) MAX(MIN(a, maxV), minV)

float from_sRGB(float srgb_color) {
    //return srgb_color;
    const float a = 0.055;

    if (srgb_color < 0.04045)
        return srgb_color / 12.92;
    else
        return std::pow((srgb_color + a) / (1.0 + a), 2.4);
}

void printProgress(const float& progress) {
    int barWidth = 44;
    std::cout << "Loading BTF: [";
    int pos = barWidth * progress;
    for (int i = 0; i < barWidth; ++i) {
        if (i <= pos) std::cout << "+";
        else std::cout << " ";
    }
    std::cout << "] (" << int(progress * 100.0) << " %)\r";
    std::cout.flush();
}


size_t BTFCache::offset_from_theta_phi_idx(size_t theta_idx, size_t phi_idx) {
    static const size_t offset[7] = {0, 1, 7, 19, 37, 57, 81};

    return offset[theta_idx] + phi_idx;
}

/*****************************************************************************
 * UTIA BIG format support
 ****************************************************************************/

BTFCacheBIG::BTFCacheBIG(const char *bigFile)
    : BTFCache ()
    , m_big(new TBIG())
{
    m_big->load(bigFile);
}

BTFCacheBIG::~BTFCacheBIG() {
    delete m_big;
}

void BTFCacheBIG::get_from_cache(size_t x, size_t y,
                                 size_t theta_i_idx, size_t phi_i_idx,
                                 size_t theta_v_idx, size_t phi_v_idx,
                                 float rgb[]) {
    const size_t idx = 81 *
        offset_from_theta_phi_idx(theta_i_idx, phi_i_idx) +
        offset_from_theta_phi_idx(theta_v_idx, phi_v_idx);

    m_big->get_pixel(0, x, y, idx, rgb);

    for (int i = 0; i < 3; i++) {
        rgb[i] = (rgb[i] > 0) ? rgb[i] / 255.0 : 0;
    }
}

size_t BTFCacheBIG::width() const {
	// /!\ bug in the TBIG class: switch between width and height
	return m_big->get_height();
    //return m_big->get_width();
}

size_t BTFCacheBIG::height() const {
	// /!\ bug in the TBIG class: switch between width and height
	return m_big->get_width();
    //return m_big->get_height();
}

/*****************************************************************************
 * Abstract class to handle BTF stored as images format support
 ****************************************************************************/

BTFImageCache::BTFImageCache(const char *rootdir,
                             long size_cache_width,
                             long size_cache_height)
    : BTFCache()
    , m_cached_img(81*81, nullptr)
    , m_len_rootdir(strlen(rootdir))
    , m_rootdir(nullptr)
    , m_cache_width(size_cache_width), m_cache_height(size_cache_height)

{
    m_rootdir = new char[m_len_rootdir + 1];
    strncpy(m_rootdir, rootdir, m_len_rootdir + 1);
}

BTFImageCache::~BTFImageCache() {
    for (std::vector<float*>::iterator it = m_cached_img.begin();
         it != m_cached_img.end(); ++it) {
        if (*it != nullptr) {
            delete[] *it;
        }
    }

    delete[] m_rootdir;
}

void BTFImageCache::get_from_cache(size_t x, size_t y,
                                   size_t theta_i_idx, size_t phi_i_idx,
                                   size_t theta_v_idx, size_t phi_v_idx,
                                   float rgb[]) {
	const size_t idx = 81 *
        offset_from_theta_phi_idx(theta_i_idx, phi_i_idx) +
        offset_from_theta_phi_idx(theta_v_idx, phi_v_idx);

    #pragma omp critical
    {
        if (m_cached_img.at(idx) == nullptr) {
            try {
                m_cached_img[idx] = load_img(theta_i_idx, phi_i_idx,
                                             theta_v_idx, phi_v_idx);
            } catch (int e) {
                if ( e == -1 ) {
                    std::cerr << "Failed  to load the file for" << std::endl
                              << "theta_i_idx=" << theta_i_idx
                              << " phi_i_idx=" << phi_i_idx << std::endl
                              << "theta_v_idx=" << theta_v_idx
                              << " phi_v_idx=" << phi_v_idx << std::endl;
                    exit(1);
                }
            }

        }
    }

    //return m_cached_img.at(idx);
    memcpy(rgb, &m_cached_img.at(idx)[4 * (y * m_cache_width + x)], 3*sizeof(float));
}

char* BTFImageCache::get_img_name_utia(size_t theta_i_idx, size_t phi_i_idx,
                                       size_t theta_v_idx, size_t phi_v_idx,
                                       const char* ext) {
    static const size_t nti = 6, ntv = 6;
    static const size_t npi[6] = {1, 6, 12, 18, 20, 24};
    static const size_t npv[6] = {1, 6, 12, 18, 20, 24};

    size_t size_ret = m_len_rootdir + 1 + 40;
    char* ret = new char[size_ret];

    const size_t theta_i = (theta_i_idx * 90) / nti;
    const size_t theta_v = (theta_v_idx * 90) / ntv;
    const size_t phi_i = (phi_i_idx * 360) / npi[theta_i_idx];
    const size_t phi_v = (phi_v_idx * 360) / npv[theta_v_idx];

    snprintf(ret, size_ret, "%s/tv%03zu_pv%03zu/tl%03zu_pl%03zu_tv%03zu_pv%03zu.%s",
             m_rootdir, theta_v, phi_v, theta_i, phi_i, theta_v, phi_v, ext);
    //printf("%s\n", ret);

    return ret;
}

char* BTFImageCache::get_img_name_ubo(size_t theta_i_idx, size_t phi_i_idx,
                                      size_t theta_v_idx, size_t phi_v_idx) {
    static const size_t nti = 6, ntv = 6;
    static const size_t npi[6] = {1, 6, 12, 18, 20, 24};
    static const size_t npv[6] = {1, 6, 12, 18, 20, 24};
    static const size_t offset[7] = {0, 1, 7, 19, 37, 57, 81};

    size_t size_ret = m_len_rootdir + 1 + 40 + 6;
    char* ret = new char[size_ret];

    const size_t nr = offset[ntv] * (offset[theta_i_idx] + phi_i_idx)
        + offset[theta_v_idx] + phi_v_idx;

    const size_t theta_i = (theta_i_idx * 90) / nti;
    const size_t theta_v = (theta_v_idx * 90) / ntv;
    const size_t phi_i = (phi_i_idx * 360) / npi[theta_i_idx];
    const size_t phi_v = (phi_v_idx * 360) / npv[theta_v_idx];

    snprintf(ret, size_ret, "%s/tv%03zu_pv%03zu/%05zu tl%03zu pl%03zu tv%03zu pv%03zu.jpg",
             m_rootdir, theta_v, phi_v, nr, theta_i, phi_i, theta_v, phi_v);
    //printf("%s\n", ret);

    return ret;
}

/*****************************************************************************
 * UTIA PNG format support
 ****************************************************************************/

BTFCachePNG::BTFCachePNG(const char* rootdir,
                         long size_cache_width, long size_cache_height)
    : BTFImageCache (rootdir, size_cache_width, size_cache_height)
{
    size_t width, height;
    get_size_from_image(width, height);

    if (size_cache_width < 0) {
        m_cache_width = width;
    }

    if (size_cache_height < 0) {
        m_cache_height = height;
    }

    m_cache_width = MIN(m_cache_width, width);
    m_cache_height = MIN(m_cache_height, height);
}

void BTFCachePNG::get_size_from_image(size_t& width, size_t& height) {
    char * img_name = get_img_name_utia(0, 0,
                                        0, 0,
                                        "png");
    std::vector<unsigned char> image;
    unsigned w, h;
    unsigned error = lodepng::decode(image, w, h, img_name);
    delete[] img_name;

    if (error != 0) throw error;

    width = w; height = h;
}

float* BTFCachePNG::load_img(size_t theta_i_idx, size_t phi_i_idx,
                             size_t theta_v_idx, size_t phi_v_idx) {
    char * img_name = get_img_name_utia(theta_i_idx, phi_i_idx,
                                        theta_v_idx, phi_v_idx,
                                        "png");

    if ( access(img_name, F_OK) == -1 ) {
        throw -1;
    }

    std::vector<unsigned char> image;
    unsigned w, h;
    unsigned error = lodepng::decode(image, w, h, img_name);
    delete[] img_name;

    const size_t wc = m_cache_width, hc = m_cache_height;

    if (error == 0) {
        float *btf = new float[wc*hc*4];

#pragma omp parallel for
        for (size_t y = 0; y < hc; y++) {
            for (size_t x = 0; x < wc; x++) {
                for (size_t c = 0; c < 3; c++) {
                    btf[4 * wc * y + 4 * x + c] = from_sRGB((float)(image[4 * w * y + 4 * x + c]) / 255.0);
                }
            }
        }

        return btf;
    } else {
        throw -2;
    }
}

/*****************************************************************************
 * UTIA EXR format support
 ****************************************************************************/

BTFCacheEXR::BTFCacheEXR(const char* rootdir,
                         long size_cache_width, long size_cache_height)
    : BTFImageCache (rootdir, size_cache_width, size_cache_height)
{
    size_t width, height;
    get_size_from_image(width, height);

    if (size_cache_width < 0) {
        m_cache_width = width;
    }

    if (size_cache_height < 0) {
        m_cache_height = height;
    }

    m_cache_width = MIN(m_cache_width, width);
    m_cache_height = MIN(m_cache_height, height);
}

void BTFCacheEXR::get_size_from_image(size_t& width, size_t& height) {
    char * img_name = get_img_name_utia(0, 0,
                                        0, 0,
                                        "exr");
    Imf::RgbaInputFile file(img_name);
    const Imath::Box2i dw = file.dataWindow();
    width = (size_t)(dw.max.x - dw.min.x + 1);
    height = (size_t)(dw.max.y - dw.min.y + 1);

    delete[] img_name;
}

float* BTFCacheEXR::load_img(size_t theta_i_idx, size_t phi_i_idx,
                             size_t theta_v_idx, size_t phi_v_idx) {
    char * img_name = get_img_name_utia(theta_i_idx, phi_i_idx,
                                        theta_v_idx, phi_v_idx,
                                        "exr");

    if ( access(img_name, F_OK) == -1 ) {
        throw -1;
    }

    Imf::Array2D<Imf::Rgba> pixels;
    Imf::RgbaInputFile file(img_name);
    const Imath::Box2i dw = file.dataWindow();
    const size_t w = (size_t)(dw.max.x - dw.min.x + 1);
    const size_t h = (size_t)(dw.max.y - dw.min.y + 1);
    pixels.resizeErase((long)(h), (long)(w));
    file.setFrameBuffer(&pixels[0][0] - dw.min.x - dw.min.y * w, 1, w);
    file.readPixels(dw.min.y, dw.max.y);

    float *btf = new float[w*h*4];

    const size_t wc = m_cache_width, hc = m_cache_height;

    for (size_t y = 0; y < hc; y++) {
        for (size_t x = 0; x < wc; x++) {
            // The division there is made for the way UTIA files are encoded
            btf[4 * wc * y + 4 * x + 0] = (float)(pixels[(long)(y)][x].r) / 255.0;
            btf[4 * wc * y + 4 * x + 1] = (float)(pixels[(long)(y)][x].g) / 255.0;
            btf[4 * wc * y + 4 * x + 2] = (float)(pixels[(long)(y)][x].b) / 255.0;
        }
    }

    delete[] img_name;

    return btf;
}

/*****************************************************************************
 * UBO JPG format support
 ****************************************************************************/

BTFCacheJPG::BTFCacheJPG(const char* rootdir,
                         long size_cache_width, long size_cache_height)
    : BTFImageCache (rootdir, size_cache_width, size_cache_height)
{
    size_t width, height;
    get_size_from_image(width, height);

    if (size_cache_width < 0) {
        m_cache_width = width;
    }

    if (size_cache_height < 0) {
        m_cache_height = height;
    }

    m_cache_width = MIN(m_cache_width, width);
    m_cache_height = MIN(m_cache_height, height);
}

void BTFCacheJPG::get_size_from_image(size_t& width, size_t& height) {
    char * img_name = get_img_name_ubo(0, 0,
                                       0, 0);
    try {
        cimg_library::CImg<unsigned char> image(img_name);

        width = static_cast<size_t>(image.width());
        height = static_cast<size_t>(image.height());
    } catch (cimg_library::CImgIOException &e) {
        delete[] img_name;

        std::cerr << "Error..." << std::endl;
        std::cerr << e.what() << std::endl;

        throw e;
    }

    delete[] img_name;
}

float* BTFCacheJPG::load_img(size_t theta_i_idx, size_t phi_i_idx,
                             size_t theta_v_idx, size_t phi_v_idx) {
    // special for jpg
    char * img_name = get_img_name_ubo(theta_i_idx, phi_i_idx,
                                       theta_v_idx, phi_v_idx);

    if ( access(img_name, F_OK) == -1 ) {
        std::cerr << "Error... Cannot access the file." << std::endl;
        throw -1;
    }

    try {
        cimg_library::CImg<unsigned char> image(img_name);

        const size_t w = static_cast<size_t>(image.width());
        const size_t h = static_cast<size_t>(image.height());

        const size_t wc = m_cache_width, hc = m_cache_height;
        float *btf = new float[wc*hc*4];

        unsigned char* pixels = image.data();
#pragma omp parallel for
        for (size_t y = 0; y < hc; y++) {
            for (size_t x = 0; x < wc; x++) {
                for (size_t c = 0; c < 3; c++) {
                    btf[4 * wc * y + 4 * x + c] = from_sRGB((float)(pixels[c * h * w + y * w + x]) / 255.0);
                }
            }
        }

        delete[] img_name;

        return btf;

    } catch (cimg_library::CImgIOException &e) {
        delete[] img_name;

        std::cerr << "Error..." << std::endl;
        std::cerr << e.what() << std::endl;

        throw e;
    }
}

BTFLoad::BTFLoad(const char *rootdir,
                 size_t width, size_t height,
                 FileFormat fileFormat)
    : m_cache(nullptr)
    , m_width(width)
    , m_height(height)
    , m_fileFormat(fileFormat)
{
    m_nti = 6; m_ntv = 6;
    m_npi = new size_t[m_nti];
    m_npv = new size_t[m_ntv];

    m_npi[0] = 1; m_npv[0] = 1;
    m_npi[1] = 6; m_npv[1] = 6;
    m_npi[2] = 12; m_npv[2] = 12;
    m_npi[3] = 18; m_npv[3] = 18;
    m_npi[4] = 20; m_npv[4] = 20;
    m_npi[5] = 24; m_npv[5] = 24;

    switch (fileFormat) {
    case FileFormat_EXR:
        m_cache = new BTFCacheEXR(rootdir, width, height);
        break;
    case FileFormat_PNG:
        m_cache = new BTFCachePNG(rootdir, width, height);
        break;
    case FileFormat_JPG:
        m_cache = new BTFCacheJPG(rootdir, width, height);
        break;
    case FileFormat_BIG:
        m_cache = new BTFCacheBIG(rootdir);//, width, height);
    }
}

BTFLoad::~BTFLoad()
{
    delete[] m_npi; delete[] m_npv;
    delete m_cache;
}

void BTFLoad::preload() {
    if (m_fileFormat == FileFormat_BIG) {
        // No need to preload BIG files
        return;
    }

    // Calculate number of images to load for progress display
    int nViewIllu = 0, nView = 0, i = 0;

    for (size_t iti = 0; iti < m_nti; iti++) {
        nViewIllu += m_npi[iti];
    }

    for (size_t itv = 0; itv < m_ntv; itv++) {
        nView += m_npv[itv];
    }

    nViewIllu *= nView;

    // Effective load of BTF
    float rgb[3];
    for (size_t iti = 0; iti < m_nti; iti++) {
        for (size_t ipi = 0; ipi < m_npi[iti]; ipi++) {
            for (size_t itv = 0; itv < m_ntv; itv++) {
                for (size_t ipv = 0; ipv < m_npv[itv]; ipv++) {
                    m_cache->get_from_cache(0, 0, iti, ipi,
                                            itv, ipv, rgb);
                    printProgress(float(i++) / (float)nViewIllu);
                }
            }
        }
    }

    printProgress(1.0);
    std::cout << std::endl;
}

bool BTFLoad::correct_angles(float &theta_i, float &phi_i,
                             float &theta_v, float &phi_v) const {
    static const float t_max = 0.5*M_PI;
    static const float p_max = 2.0*M_PI;

    // Get the elevation values positive --> [0, +inf[
    if (theta_i < 0) {
        theta_i = -theta_i;
        phi_i = phi_i + p_max/2.0;
    }

    if (theta_v < 0) {
        theta_v = -theta_v;
        phi_v = phi_v + p_max/2.0;
    }

    // Apply modulo 2 PI on azimuth --> [0, 2*pi[
    while (phi_i < 0) {
        phi_i = phi_i + p_max;
    }

    while (phi_v < 0) {
        phi_v = phi_v + p_max;
    }

    while (phi_i >= p_max) {
        phi_i = phi_i - p_max;
    }

    while (phi_v >= p_max) {
        phi_v = phi_v - p_max;
    }

    // Check if above horizon
    return (theta_i < t_max) && (theta_v < t_max);
}

void BTFLoad::get_theta_phi_from_idx(size_t i_theta, size_t i_phi,
                                     float &theta, float &phi) const {
    static const float t_max = 0.5*M_PI;
    static const float p_max = 2.0*M_PI;

    theta = (float)(i_theta) / (float)(m_nti) * t_max;
    phi = (float)(i_phi) / (float)(m_npi[i_theta]) * p_max;
}

void BTFLoad::lookup_btf_val(size_t x, size_t y,
                             const float &theta_i, const float &phi_i,
                             const float &theta_v, const float &phi_v,
                             float RGB[]) {
    static const float t_max = 0.5*M_PI;
    static const float p_max = 2.0*M_PI;

    float th_i = theta_i;
    float ph_i = phi_i;
    float th_v = theta_v;
    float ph_v = phi_v;

    if (!correct_angles(th_i, ph_i, th_v, ph_v)) {
        memset(RGB, 0, 3 * sizeof(float));
        return;
    }

    // Theta indexes for interpolation
    size_t idx_ti[2], idx_tv[2];

    idx_ti[0] = (size_t)(CLAMP((int)(floor(m_nti * th_i / t_max)), 0, (int)(m_nti) - 2));
    idx_tv[0] = (size_t)(CLAMP((int)(floor(m_ntv * th_v / t_max)), 0, (int)(m_ntv) - 2));

    idx_ti[1] = idx_ti[0] + 1;
    idx_tv[1] = idx_tv[0] + 1;

    // Phi indexes for interpolation
    size_t idx_pi[2][2], idx_pv[2][2];

    for (size_t i = 0; i < 2; i++) {
        const size_t npi_local = m_npi[idx_ti[i]];
        const size_t npv_local = m_npv[idx_tv[i]];

        idx_pi[i][0] = (size_t)(CLAMP((int)(floor(npi_local * ph_i / p_max)),
                                      0,
                                      (int)(npi_local) - 1));

        idx_pv[i][0] = (size_t)(CLAMP((int)(floor(npv_local * ph_v / p_max)),
                                      0,
                                      (int)(npv_local) - 1));

        idx_pi[i][1] = idx_pi[i][0] + 1;
        idx_pv[i][1] = idx_pv[i][0] + 1;
    }

    // Calculate weights for interpolation
    float weight_ti[2], weight_tv[2];
    float weight_pi[2][2], weight_pv[2][2];

    for (size_t i = 0; i < 2; i++) {
        weight_ti[i] = std::abs((float)(idx_ti[i])/(float)(m_nti) - th_i/t_max);
        weight_tv[i] = std::abs((float)(idx_tv[i])/(float)(m_ntv) - th_v/t_max);

        for (size_t j = 0; j < 2; j++) {
            weight_pi[i][j] = std::abs((float)(idx_pi[i][j])/(float)(m_npi[idx_ti[i]]) - phi_i/p_max);
            weight_pv[i][j] = std::abs((float)(idx_pv[i][j])/(float)(m_npv[idx_tv[i]]) - phi_v/p_max);
        }
    }

    // Get the sum of weights
    float swti(0), swtv(0);
    float swpi[2] = {0}, swpv[2] = {0};
    for (int i = 0; i < 2; i++) {
        swti += weight_ti[i]; swtv += weight_tv[i];
        for (int j = 0; j < 2; j++) {
            swpi[i] += weight_pi[i][j];
            swpv[i] += weight_pv[i][j];
        }
    }

    for (int i = 0; i < 2; i++) {
        // Ensure the sums of weights equal 1.0
        weight_ti[i] = 1.0 - weight_ti[i] / swti;
        weight_tv[i] = 1.0 - weight_tv[i] / swtv;
        for (int j = 0; j < 2; j++) {
            weight_pi[i][j] = 1.0 - weight_pi[i][j] / swpi[i];
            weight_pv[i][j] = 1.0 - weight_pv[i][j] / swpv[i];
        }

        // Ensure the phi indices are in range
        // otherwise, cycle around
        if (idx_pi[i][1] >= m_npi[idx_ti[i]])
            idx_pi[i][1] = 0;
        if (idx_pv[i][1] >= m_npv[idx_tv[i]])
            idx_pv[i][1] = 0;
    }

    // memset
    memset(RGB, 0, 3*sizeof(float));

    // Interpolate the BTF on theta, phi
    for (size_t pi = 0; pi < 2; pi++) {
        for (size_t pv = 0; pv < 2; pv++) {
            for (size_t ti = 0; ti < 2; ti++) {
                for (size_t tv = 0; tv < 2; tv++) {
                    float refl[3] = {0};
                    lookup_btf_idx(x, y,
                                   idx_ti[ti], idx_pi[ti][pi],
                                   idx_tv[tv], idx_pv[tv][pv],
                                   refl);

                    for (int c = 0; c < 3; c++) {
                        RGB[c] += weight_ti[ti] * weight_pi[ti][pi] *
                            weight_tv[tv] * weight_pv[tv][pv] *
                            refl[c];
                    }
                }
            }
        }
    }
}

void BTFLoad::lookup_btf_val_raw(size_t x, size_t y,
                                 const float &theta_i, const float &phi_i,
                                 const float &theta_v, const float &phi_v,
                                 float RGB[]) {

    static const float t_max = 0.5*M_PI;
    static const float p_max = 2.0*M_PI;

    float th_i = theta_i;
    float ph_i = phi_i;
    float th_v = theta_v;
    float ph_v = phi_v;

    if (!correct_angles(th_i, ph_i, th_v, ph_v)) {
        memset(RGB, 0, 3 * sizeof(float));
        return;
    }

    const size_t idx_ti = CLAMP((size_t)(round(m_nti * th_i / t_max)), 0, m_nti - 1);
    const size_t idx_tv = CLAMP((size_t)(round(m_ntv * th_v / t_max)), 0, m_ntv - 1);

    const size_t idx_pi = CLAMP((size_t)(round(m_npi[idx_ti] * ph_i / p_max)), 0, m_npi[idx_ti] - 1);
    const size_t idx_pv = CLAMP((size_t)(round(m_npv[idx_tv] * ph_v / p_max)), 0, m_npv[idx_tv] - 1);

    lookup_btf_idx(x, y, idx_ti, idx_pi, idx_tv, idx_pv, RGB);
}

void BTFLoad::lookup_btf_idx(size_t x, size_t y,
                             size_t theta_i_idx, size_t phi_i_idx,
                             size_t theta_v_idx, size_t phi_v_idx,
                             float RGB[]) {
    m_cache->get_from_cache(x, y,
                            theta_i_idx, phi_i_idx,
                            theta_v_idx, phi_v_idx, RGB);
}

