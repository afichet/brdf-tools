/*
 * BTFLoad.h
 *
 *  Created on: Sep 7, 2017
 *      Author: afichet
 */

#ifndef BTFLOAD_H_
#define BTFLOAD_H_

#include <vector>
#include <cstdio>

/*****************************************************************************
 * BTF Cache
 * Abstract function to handle interface between the BTF load which handles
 * angles and the file stored on the filesystem which are indexed
 ****************************************************************************/

class BTFCache {
public:
    BTFCache() {}

    virtual ~BTFCache() {}

	static size_t offset_from_theta_phi_idx(size_t theta_idx, size_t phi_idx);
	
    virtual void get_from_cache(size_t x, size_t y,
                                size_t theta_i_idx, size_t phi_i_idx,
                                size_t theta_v_idx, size_t phi_v_idx,
                                float rgb[]) = 0;

    virtual size_t width() const = 0;

    virtual size_t height() const = 0;
};

class TBIG;

/*****************************************************************************
 * UTIA BIG format support
 ****************************************************************************/

class BTFCacheBIG : public BTFCache {
public:
    BTFCacheBIG(const char *bigFile);

    virtual ~BTFCacheBIG();

    virtual void get_from_cache(size_t x, size_t y,
                                size_t theta_i_idx, size_t phi_i_idx,
                                size_t theta_v_idx, size_t phi_v_idx,
                                float rgb[]);

    virtual size_t width() const;

    virtual size_t height() const;

private:
    TBIG* m_big;
};

/*****************************************************************************
 * Abstract class to handle BTF stored as images format support
 ****************************************************************************/

class BTFImageCache : public BTFCache {
public:
    BTFImageCache(const char *rootdir,
                  long size_cache_width = -1, long size_cache_height = -1);

    virtual ~BTFImageCache();

    virtual void get_from_cache(size_t x, size_t y,
                                size_t theta_i_idx, size_t phi_i_idx,
                                size_t theta_v_idx, size_t phi_v_idx,
                                float rgb[]);

    virtual size_t width() const {
        return m_cache_width;
    }

    virtual size_t height() const {
        return m_cache_height;
    }

protected:
    virtual void get_size_from_image(size_t& width, size_t& height) = 0;

    char* get_img_name_utia(size_t theta_i_idx, size_t phi_i_idx,
                            size_t theta_v_idx, size_t phi_v_idx,
                            const char* ext);

    char* get_img_name_ubo(size_t theta_i_idx, size_t phi_i_idx,
                           size_t theta_v_idx, size_t phi_v_idx);

    virtual float* load_img(size_t theta_i_idx, size_t phi_i_idx,
                            size_t theta_v_idx, size_t phi_v_idx) = 0;

    std::vector<float*> m_cached_img;
    size_t m_len_rootdir;
    char* m_rootdir;
    size_t m_cache_width, m_cache_height;
};

/*****************************************************************************
 * UTIA PNG format support
 ****************************************************************************/

class BTFCachePNG : public BTFImageCache {
public:
    BTFCachePNG(const char* rootdir,
                long size_cache_width = -1, long size_cache_height = -1);

protected:
    virtual void get_size_from_image(size_t& width, size_t& height);

    virtual float* load_img(size_t theta_i_idx, size_t phi_i_idx,
                            size_t theta_v_idx, size_t phi_v_idx);
};

/*****************************************************************************
 * UTIA EXR format support
 ****************************************************************************/

class BTFCacheEXR : public BTFImageCache {
public:
    BTFCacheEXR(const char* rootdir,
                long size_cache_width = -1, long size_cache_height = -1);

protected:
    virtual void get_size_from_image(size_t& width, size_t& height);

    virtual float* load_img(size_t theta_i_idx, size_t phi_i_idx,
                            size_t theta_v_idx, size_t phi_v_idx);
};

/*****************************************************************************
 * UBO JPG format support
 ****************************************************************************/

class BTFCacheJPG : public BTFImageCache {
public:
    BTFCacheJPG(const char* rootdir,
                long size_cache_width = -1, long size_cache_height = -1);

protected:
    virtual void get_size_from_image(size_t& width, size_t& height);

    virtual float* load_img(size_t theta_i_idx, size_t phi_i_idx,
                            size_t theta_v_idx, size_t phi_v_idx);
};

class BTFLoad {
public:
    enum FileFormat {
        FileFormat_EXR,
        FileFormat_PNG,
        FileFormat_JPG,
        FileFormat_BIG
    };

    BTFLoad(const char *rootdir,
            size_t width, size_t height,
            FileFormat fileFormat = FileFormat_PNG);

    virtual ~BTFLoad();

    void preload();

    void get_theta_phi_from_idx(size_t i_theta, size_t i_phi,
                                float &theta, float &phi) const;

    bool correct_angles(float &theta_i, float &phi_i,
                        float &theta_v, float &phi_v) const;

    virtual void lookup_btf_val(size_t x, size_t y,
                                const float &theta_i, const float &phi_i,
                                const float &theta_v, const float &phi_v,
                                float RGB[]);

    virtual void lookup_btf_val_raw(size_t x, size_t y,
                                    const float &theta_i, const float &phi_i,
                                    const float &theta_v, const float &phi_v,
                                    float RGB[]);

    virtual void lookup_btf_idx(size_t x, size_t y,
                                size_t theta_i_idx, size_t phi_i_idx,
                                size_t theta_v_idx, size_t phi_v_idx,
                                float RGB[]);

    size_t get_nti() const {
        return m_nti;
    }

    size_t get_ntv() const {
        return m_ntv;
    }

    const size_t* get_npi() const {
        return m_npi;
    }

    const size_t* get_npv() const {
        return m_npv;
    }

    size_t width() const {
        return m_cache->width();
    }

    size_t height() const {
        return m_cache->height();
    }

private:
    size_t m_nti, m_ntv;
    size_t *m_npi, *m_npv;
    BTFCache* m_cache;
    size_t m_width, m_height;
    FileFormat m_fileFormat;
};

#endif /* BTFLOAD_H_ */

