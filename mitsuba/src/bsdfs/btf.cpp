#include <mitsuba/core/fresolver.h>
#include <mitsuba/render/bsdf.h>
#include <mitsuba/render/texture.h>
#include <mitsuba/hw/basicshader.h>
#include <mitsuba/core/warp.h>

#include <boost/algorithm/string.hpp>

#include "3rdparty/BTF/BTFLoad.h"

MTS_NAMESPACE_BEGIN

static void xyz_to_theta_phi(const Vector& p, Float *theta, Float *phi) {
    if (p.z > 0.99999) {
        (*theta) = (*phi) = 0.0;
    } else if (p.z < -0.99999) {
        (*theta) = M_PI;
        (*phi) = 0.0;
    } else {
        (*theta) = acos(p.z);
        (*phi) = atan2(p.y, p.x);
    }
}

/*!\plugin{BTF}{BTF measured material}
 */
class BTF: public BSDF {
public:
    BTF(const Properties &props)
        : BSDF(props) {
		fs::path filename =
			Thread::getThread()->getFileResolver()->resolve(props.getString("filename"));

        std::string type = boost::to_lower_copy(props.getString("format", "png"));

        m_scale = props.getFloat("scale", 1);
        m_width  = (size_t)props.getFloat("width", -1);
        m_height = (size_t)props.getFloat("height", -1);
        
        if (type == "png") {
            m_btf = new BTFLoad(filename.string().c_str(),
                                m_width, m_height,
                                BTFLoad::FileFormat_PNG);
        } else if (type == "exr") {
            m_btf = new BTFLoad(filename.string().c_str(),
                                m_width, m_height,
                                BTFLoad::FileFormat_EXR);
        } else if (type == "jpg") {
            m_btf = new BTFLoad(filename.string().c_str(),
                                m_width, m_height,
                                BTFLoad::FileFormat_JPG);
        } else if (type == "big") {
            m_btf = new BTFLoad(filename.string().c_str(),
                                m_width, m_height,
                                BTFLoad::FileFormat_BIG);
        } else {
            Log(EError, "Specified an invalid file type \"%s\", must be "
                "\"png\", \"exr\", or \"jpg\"!", type.c_str());
        }

        m_width = m_btf->width();
        m_height = m_btf->height();

        // A preload is preferable here...
        m_btf->preload();
    }

    BTF(Stream *stream, InstanceManager *manager)
        : BSDF(stream, manager) {
        configure();
		/*
          size_t len = 3 * BRDF_SAMPLING_RES_THETA_H *
          BRDF_SAMPLING_RES_THETA_D *
          BRDF_SAMPLING_RES_PHI_D / 2;
		
          m_brdf = new double[len];
		
          stream->readDoubleArray(m_brdf, len);
		*/
    }

    ~BTF() {
        delete m_btf;
    }

    void configure() {
        m_components.clear();     
        m_components.push_back(EDiffuseReflection | EFrontSide | 0);
        m_usesRayDifferentials = false;
        BSDF::configure();
    }

    void getXY(const BSDFSamplingRecord &bRec, size_t &x, size_t &y) const {
        Float u = m_scale * bRec.its.uv.x;
        Float v = m_scale * bRec.its.uv.y;
        
        u = u - floor(u);
        v = v - floor(v);
		
        x = static_cast<size_t>(u * static_cast<Float>(m_width));
        y = static_cast<size_t>(v * static_cast<Float>(m_height));
    }
	
    Spectrum eval(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        if (!(bRec.typeMask & EDiffuseReflection) || measure != ESolidAngle
            || Frame::cosTheta(bRec.wi) <= 0
            || Frame::cosTheta(bRec.wo) <= 0)
            return Spectrum(0.0f);

        Float theta_i, phi_i, theta_o, phi_o;
        xyz_to_theta_phi(bRec.wi, &theta_i, &phi_i);
        xyz_to_theta_phi(bRec.wo, &theta_o, &phi_o);
        
        size_t x, y;
        getXY(bRec, x, y);
        
        float rgb[3];
        //m_btf->lookup_btf_val_raw(x, y, theta_i, phi_i, theta_o, phi_o, rgb);
        m_btf->lookup_btf_val(x, y, theta_i, phi_i, theta_o, phi_o, rgb);
		
        return Color3(rgb[0], rgb[1], rgb[2])
            * (INV_PI * Frame::cosTheta(bRec.wo));
    }

    Float pdf(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        if (!(bRec.typeMask & EDiffuseReflection) || measure != ESolidAngle
            || Frame::cosTheta(bRec.wi) <= 0
            || Frame::cosTheta(bRec.wo) <= 0)
            return 0.0f;

        return warp::squareToCosineHemispherePdf(bRec.wo);
    }

    Spectrum sample(BSDFSamplingRecord &bRec, const Point2 &sample) const {
        if (!(bRec.typeMask & EDiffuseReflection) || Frame::cosTheta(bRec.wi) <= 0)
            return Spectrum(0.0f);

        bRec.wo = warp::squareToCosineHemisphere(sample);
        bRec.eta = 1.0f;
        bRec.sampledComponent = 0;
        bRec.sampledType = EDiffuseReflection;
		
        Float theta_i, phi_i, theta_o, phi_o;
        xyz_to_theta_phi(bRec.wi, &theta_i, &phi_i);
        xyz_to_theta_phi(bRec.wo, &theta_o, &phi_o);

        size_t x, y;
        getXY(bRec, x, y);
        
        float rgb[3];
        //m_btf->lookup_btf_val_raw(x, y, theta_i, phi_i, theta_o, phi_o, rgb);
        m_btf->lookup_btf_val(x, y, theta_i, phi_i, theta_o, phi_o, rgb);
				
        return Color3(rgb[0], rgb[1], rgb[2]);
    }

    Spectrum sample(BSDFSamplingRecord &bRec, Float &pdf, const Point2 &sample) const {
        if (!(bRec.typeMask & EDiffuseReflection) || Frame::cosTheta(bRec.wi) <= 0)
            return Spectrum(0.0f);

        bRec.wo = warp::squareToCosineHemisphere(sample);
        bRec.eta = 1.0f;
        bRec.sampledComponent = 0;
        bRec.sampledType = EDiffuseReflection;
        pdf = warp::squareToCosineHemispherePdf(bRec.wo);

        Float theta_i, phi_i, theta_o, phi_o;
        xyz_to_theta_phi(bRec.wi, &theta_i, &phi_i);
        xyz_to_theta_phi(bRec.wo, &theta_o, &phi_o);

        size_t x, y;
        getXY(bRec, x, y);

        float rgb[3];		
        //m_btf->lookup_btf_val_raw(x, y, theta_i, phi_i, theta_o, phi_o, rgb);
        m_btf->lookup_btf_val(x, y, theta_i, phi_i, theta_o, phi_o, rgb);		
				
        return Color3(rgb[0], rgb[1], rgb[2]);
    }

    void addChild(const std::string &name, ConfigurableObject *child) {
		BSDF::addChild(name, child);
    }

    void serialize(Stream *stream, InstanceManager *manager) const {
        BSDF::serialize(stream, manager);
		
		/*
          size_t len = 3 * BRDF_SAMPLING_RES_THETA_H *
          BRDF_SAMPLING_RES_THETA_D *
          BRDF_SAMPLING_RES_PHI_D / 2;

          stream->writeDoubleArray(m_brdf, len);
		*/
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << "BTF[" << endl
            << "  id = \""   << getID() << "\"," << endl
            << "  width = "  << m_width  << ","  << endl
            << "  height = " << m_height << ","  << endl
            << "]";
        return oss.str();
    }

    Shader *createShader(Renderer *renderer) const;

    MTS_DECLARE_CLASS()
private:
    BTFLoad* m_btf;
	size_t m_width, m_height;
	Float m_scale;
};

// ================ Hardware shader implementation ================

class BTFShader : public Shader {
public:
    BTFShader(Renderer *renderer)
        : Shader(renderer, EBSDFShader)
		, m_reflectance(new ConstantSpectrumTexture(Spectrum(.5f))) {
        m_reflectanceShader = renderer->registerShaderForResource(m_reflectance.get());
    }

    bool isComplete() const {
        return m_reflectanceShader.get() != NULL;
    }

    void cleanup(Renderer *renderer) {
        renderer->unregisterShaderForResource(m_reflectance.get());
    }

    void putDependencies(std::vector<Shader *> &deps) {
        deps.push_back(m_reflectanceShader.get());
    }

    void generateCode(std::ostringstream &oss,
                      const std::string &evalName,
                      const std::vector<std::string> &depNames) const {
        oss << "vec3 " << evalName << "(vec2 uv, vec3 wi, vec3 wo) {" << endl
            << "    if (cosTheta(wi) < 0.0 || cosTheta(wo) < 0.0)" << endl
            << "        return vec3(0.0);" << endl
            << "    return " << depNames[0] << "(uv) * inv_pi * cosTheta(wo);" << endl
            << "}" << endl
            << endl
            << "vec3 " << evalName << "_diffuse(vec2 uv, vec3 wi, vec3 wo) {" << endl
            << "    return " << evalName << "(uv, wi, wo);" << endl
            << "}" << endl;
    }

    MTS_DECLARE_CLASS()
private:
    ref<const Texture> m_reflectance;
    ref<Shader> m_reflectanceShader;
};

Shader *BTF::createShader(Renderer *renderer) const {
    return new BTFShader(renderer);
}

MTS_IMPLEMENT_CLASS(BTFShader, false, Shader)
    MTS_IMPLEMENT_CLASS_S(BTF, false, BSDF)
    MTS_EXPORT_PLUGIN(BTF, "Measured BTF")
    MTS_NAMESPACE_END
