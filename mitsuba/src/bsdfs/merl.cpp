/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob and others.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include <mitsuba/core/fresolver.h>
#include <mitsuba/render/bsdf.h>
#include <mitsuba/render/texture.h>
#include <mitsuba/hw/basicshader.h>
#include <mitsuba/core/warp.h>
#include "3rdparty/MERL/BRDFRead.cpp"

MTS_NAMESPACE_BEGIN

static void xyz_to_theta_phi(const Vector& p, Float *theta, Float *phi) {
	if (p.z > 0.99999) {
		(*theta) = (*phi) = 0.0;
	} else if (p.z < -0.99999) {
		(*theta) = M_PI;
		(*phi) = 0.0;
	} else {
		(*theta) = acos(p.z);
		(*phi) = atan2(p.y, p.x);
	}
}


/*!\plugin{MERL}{MERL measured material}
 */
class MERL : public BSDF {
public:
    MERL(const Properties &props)
        : BSDF(props) {
		fs::path filename =
			Thread::getThread()->getFileResolver()->resolve(props.getString("filename"));

		// Load MERL
		if (!read_brdf(filename.string().c_str(), m_brdf)) {
			std::cerr << "Error reading " << filename.string() << std::endl;
			exit(1);
		}																			 
    }

    MERL(Stream *stream, InstanceManager *manager)
        : BSDF(stream, manager) {
        configure();
		
		size_t len = 3 * BRDF_SAMPLING_RES_THETA_H *
			BRDF_SAMPLING_RES_THETA_D *
			BRDF_SAMPLING_RES_PHI_D / 2;
		
		m_brdf = new double[len];
		
		stream->readDoubleArray(m_brdf, len);
    }

	~MERL() {
		delete[] m_brdf;
	}

    void configure() {
        m_components.clear();     
		m_components.push_back(EDiffuseReflection | EFrontSide | 0);
		m_usesRayDifferentials = false;
        BSDF::configure();
    }

    Spectrum eval(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        if (!(bRec.typeMask & EDiffuseReflection) || measure != ESolidAngle
            || Frame::cosTheta(bRec.wi) <= 0
            || Frame::cosTheta(bRec.wo) <= 0)
            return Spectrum(0.0f);

		Float theta_i, phi_i, theta_o, phi_o;
		xyz_to_theta_phi(bRec.wi, &theta_i, &phi_i);
		xyz_to_theta_phi(bRec.wo, &theta_o, &phi_o);

		double r, g, b;
		lookup_brdf_val(m_brdf, theta_i, phi_i, theta_o, phi_o, r, g, b);
		
        return Color3(r, g, b)
            * (INV_PI * Frame::cosTheta(bRec.wo));
    }

    Float pdf(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        if (!(bRec.typeMask & EDiffuseReflection) || measure != ESolidAngle
            || Frame::cosTheta(bRec.wi) <= 0
            || Frame::cosTheta(bRec.wo) <= 0)
            return 0.0f;

        return warp::squareToCosineHemispherePdf(bRec.wo);
    }

    Spectrum sample(BSDFSamplingRecord &bRec, const Point2 &sample) const {
        if (!(bRec.typeMask & EDiffuseReflection) || Frame::cosTheta(bRec.wi) <= 0)
            return Spectrum(0.0f);

        bRec.wo = warp::squareToCosineHemisphere(sample);
        bRec.eta = 1.0f;
        bRec.sampledComponent = 0;
        bRec.sampledType = EDiffuseReflection;

		Float theta_i, phi_i, theta_o, phi_o;
		xyz_to_theta_phi(bRec.wi, &theta_i, &phi_i);
		xyz_to_theta_phi(bRec.wo, &theta_o, &phi_o);
		
		double r, g, b;
		lookup_brdf_val(m_brdf, theta_i, phi_i, theta_o, phi_o, r, g, b);
		
        return Color3(r, g, b);
    }

    Spectrum sample(BSDFSamplingRecord &bRec, Float &pdf, const Point2 &sample) const {
        if (!(bRec.typeMask & EDiffuseReflection) || Frame::cosTheta(bRec.wi) <= 0)
            return Spectrum(0.0f);

        bRec.wo = warp::squareToCosineHemisphere(sample);
        bRec.eta = 1.0f;
        bRec.sampledComponent = 0;
        bRec.sampledType = EDiffuseReflection;
        pdf = warp::squareToCosineHemispherePdf(bRec.wo);

		Float theta_i, phi_i, theta_o, phi_o;
		xyz_to_theta_phi(bRec.wi, &theta_i, &phi_i);
		xyz_to_theta_phi(bRec.wo, &theta_o, &phi_o);
		
		double r, g, b;
		lookup_brdf_val(m_brdf, theta_i, phi_i, theta_o, phi_o, r, g, b);
		
        return Color3(r, g, b);
    }

    void addChild(const std::string &name, ConfigurableObject *child) {
		BSDF::addChild(name, child);
    }

    void serialize(Stream *stream, InstanceManager *manager) const {
        BSDF::serialize(stream, manager);

		size_t len = 3 * BRDF_SAMPLING_RES_THETA_H *
			BRDF_SAMPLING_RES_THETA_D *
			BRDF_SAMPLING_RES_PHI_D / 2;

		stream->writeDoubleArray(m_brdf, len);
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << "MERL[" << endl
            << "  id = \"" << getID() << "\"," << endl
            << "]";
        return oss.str();
    }

    Shader *createShader(Renderer *renderer) const;

    MTS_DECLARE_CLASS()
private:
	double* m_brdf;
};

// ================ Hardware shader implementation ================

class MERLShader : public Shader {
public:
    MERLShader(Renderer *renderer)
        : Shader(renderer, EBSDFShader)
		, m_reflectance(new ConstantSpectrumTexture(Spectrum(.5f))) {
        m_reflectanceShader = renderer->registerShaderForResource(m_reflectance.get());
    }

    bool isComplete() const {
        return m_reflectanceShader.get() != NULL;
    }

    void cleanup(Renderer *renderer) {
        renderer->unregisterShaderForResource(m_reflectance.get());
    }

    void putDependencies(std::vector<Shader *> &deps) {
        deps.push_back(m_reflectanceShader.get());
    }

    void generateCode(std::ostringstream &oss,
            const std::string &evalName,
            const std::vector<std::string> &depNames) const {
        oss << "vec3 " << evalName << "(vec2 uv, vec3 wi, vec3 wo) {" << endl
            << "    if (cosTheta(wi) < 0.0 || cosTheta(wo) < 0.0)" << endl
            << "        return vec3(0.0);" << endl
            << "    return " << depNames[0] << "(uv) * inv_pi * cosTheta(wo);" << endl
            << "}" << endl
            << endl
            << "vec3 " << evalName << "_diffuse(vec2 uv, vec3 wi, vec3 wo) {" << endl
            << "    return " << evalName << "(uv, wi, wo);" << endl
            << "}" << endl;
    }

    MTS_DECLARE_CLASS()
private:
    ref<const Texture> m_reflectance;
    ref<Shader> m_reflectanceShader;
};

Shader *MERL::createShader(Renderer *renderer) const {
    return new MERLShader(renderer);
}

MTS_IMPLEMENT_CLASS(MERLShader, false, Shader)
MTS_IMPLEMENT_CLASS_S(MERL, false, BSDF)
MTS_EXPORT_PLUGIN(MERL, "MERL BRDF")
MTS_NAMESPACE_END
